import React from "react";
import Grid from '@mui/material/Grid'; 
import './css.css'
import Details from "../components/Details";
import Actions from "../components/Actions";
import Owners from "../components/Owners";
import QRCode from "react-qr-code";



export default function HomePage() {

  const [value, setValue] = React.useState("1");

  const handleChange = (e) => {
    setValue(e);
  };

console.log(value)
  return (
    <div>
      <Grid container>
        <Grid item xs={6} className='leftHandSide' >
          <div className="lhs1">
            
          </div>
          <div className="lhs2" style={{
            // backgroundImage: "url(" + { Background } + ")",
            height: '100 %',
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover'
          }}>
            <h2 className="Headding" style={{marginTop : '80px'}}>Above and Beyond Individual Award</h2>
            <h2 className="Headding">Name</h2>
            <h6 className="Headding">Pellentesque habitant morbi tristique senectus et netus et malesuada
              fames ac turpis egestas. Vestibulum tortor quam,
              feugiat vitae, ultricies eget, tempor sit amet, ante.
            </h6>
            <h6 className="Headding">Pellentesque habitant morbi tristique senectus et netus et malesuada
              fames ac turpis egestas. Vestibulum tortor quam,
              feugiat vitae, ultricies eget, tempor sit amet, ante.
            </h6>
            <h6 className="Headding">Pellentesque habitant morbi tristique senectus et netus et malesuada
              fames ac turpis egestas. Vestibulum tortor quam,
              feugiat vitae, ultricies eget, tempor sit amet, ante.
            </h6>
            <Grid container >
            <Grid item xs={6}> 
            <h4 className="Headding">From : Arnab Basu</h4>
            <h6 className="Headding">Date : </h6>
            </Grid>
            <Grid item xs={5}> 
            <QRCode
                      value="http://google.com"
                      size="100"
                      style={{ float: "right" }}
                    />
            </Grid>
            </Grid>
            <h6 className="mintdate">
              This Nft was minted on 21-10-2022
            </h6>
           
            
          </div>
          <div>
            <Grid container className="lhs3">
              <Grid><button>Download</button></Grid>
              <Grid className="lhs3item" style={{marginLeft : '20vw'}}>Live</Grid>
              <Grid className="lhs3item">Explore</Grid>
              <Grid className="lhs3item">Roadmap</Grid>
              <Grid className="lhs3item">Status</Grid>
              <Grid className="lhs3item">Api</Grid>
              <Grid className="lhs3item">Faq</Grid>
            </Grid>
          </div>
        </Grid>
        <Grid item xs={6} className='rightHandSide' >
          {/* <Grid container>
            <Grid item xs={3}>
              <div onClick={() => handleChange(1)} className={value == 1 ? 'selectTapSelected' : 'selectTap'} style={{ marginLeft: '30px' }}>
                Details
              </div>
            </Grid>
            <Grid item xs={3}>
              <div onClick={() => handleChange(2)} className={value === 2 ? 'selectTapSelected' : 'selectTap'} >
                Actions
              </div>
            </Grid>
            <Grid item xs={3}>
              <div onClick={() => handleChange(3)} className={value === 3 ? 'selectTapSelected' : 'selectTap'} >
                Owners
              </div>
            </Grid>
          </Grid> */}
          {/* {value == 1 ? <Details /> : value == 2 ? <Actions /> : <Owners />} */}
          <Details/>
        </Grid>
      </Grid>
    </div >
  );
}
