import CssBaseline from "@material-ui/core/CssBaseline";
import Routes from "./Routes";


function App() {
  return (
    <div>
      <CssBaseline />
      <Routes />
    </div>
  );
}

export default App;
