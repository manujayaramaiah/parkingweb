import React,{useState} from 'react'
import Card from '@mui/material/Card';
import { Button } from "@mui/material";
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import InputAdornment from '@mui/material/InputAdornment';
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import TwoWheelerIcon from '@mui/icons-material/TwoWheeler';

import './Details.css'



function Details() {

  // const [category, setcategory] = useState('')


  return (
    <div>

      <div className="cardHolder">

        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Card >
            <CardHeader
              avatar={
                <Avatar
                  sx={{ bgcolor: '#ffebee' }} aria-label="recipe"
                  style={{ fontSize: '20px', margin: '1vh 0 0 15vw' }}
                >
                  R
                </Avatar>
              }
            />
            <CardContent style={{ textAlign: 'center' }}>
              <div style={{ fontSize: '15px', display: 'inline-block' }} >
                Welcome Back !!! name
              </div>
              <div style={{ fontSize: '15px',  display: 'inline-block' }} >
                Please Select The Category
              </div>
              <div style={{ fontSize: '15px',  marginTop : '3vh' }} >
              <DirectionsCarIcon 
              // onClick= {setcategory('car')}
              style={{ marginRight : '3vh' }}
              />
              <TwoWheelerIcon
                // onClick= {setcategory('bike')}
              />
              </div>
              
              <div style={{ fontSize: '15px', display: 'inline-block' , marginTop : '3vh' }} >
                Please Enter your Desired Location
              </div>
              <div style={{ fontSize: '15px',  display: 'inline-block', marginTop : '3vh' }} >

                <TextField
                size='small'
                style={{ fontSize: '15px', display: 'inline-block' }}
                  id="input-with-icon-textfield"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <LocationOnIcon />
                      </InputAdornment>
                    ),
                  }}
                  variant="outlined"
                />
               
              </div>
              <div style={{ fontSize: '15px', marginTop : '3vh' }} >
              <Button color="primary"
                  variant="contained"
                >submit</Button>
              </div>
             
            </CardContent>
          </Card>
        </Grid>

      </div>


    </div>
  )
}

export default Details